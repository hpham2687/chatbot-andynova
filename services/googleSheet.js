const { google } = require('googleapis');
const runGoogleSheet = require('./googleSheetConfig');
const fs = require("fs");
const writing9Api = require("./writing9");
const { parse } = require('path');
class GoogleSheetApi {
    getNewWord = async () => {
        return await runGoogleSheet(listMajors);
    }
    writeResultWriting = async () => {
      return await runGoogleSheet(writeWriting);
  }
}
async function writeWriting (auth){
  const sheets = google.sheets({ version: 'v4', auth });
  const currentLinePath = __dirname+'/currentline.txt';

var currentLine = fs.readFileSync(currentLinePath, 'utf8');
console.log(currentLine);


// get data 
const res = await sheets.spreadsheets.values.get({
  spreadsheetId: '15OsdoKo4siDGwi-RfSlzgLMIPM5AHeCequ7Z4aao0hA',
  range: `Sheet1!B${currentLine}:B500`,
});
if (res && res.data.values) {
  const rows = res.data.values;
  if (rows.length) {
    console.log('row length = ', rows.length);

   rows.forEach(async (item, index) => {

    let cellData = rows[index][0];
  if (typeof cellData.split('/')[1] !== "undefined"  && typeof cellData.split('/')[2] !== "undefined" && cellData.split('/')[2].length > 100 && cellData.split('/')[1].length > 50){
  var questionAnswer = {
    question: cellData.split('/')[1],
    text: cellData.split('/')[2]
  } 
  console.log(index+'thoa man');
  const result = await writing9Api.checkWriting(questionAnswer);
  console.log(result);
  writeResultToCell(sheets, parseInt(currentLine)+index, result);

  }
   })

   
fs.writeFileSync(currentLinePath, parseInt(currentLine)+rows.length); 


   
return "done";



  
    
  } else {
    return "data not found";

  }
}else{
  return "done roi"
}


 

  


}
async function writeResultToCell(sheets, row, data){

  const response = (await sheets.spreadsheets.values.update({
    spreadsheetId: '15OsdoKo4siDGwi-RfSlzgLMIPM5AHeCequ7Z4aao0hA',

     range: `M${row}`,  // TODO: Update placeholder value.

     // How the input data should be interpreted.
     valueInputOption: 'RAW',  // TODO: Update placeholder value.
 
     resource: 
     {
      "values": [
        [
          data
        ],
      ],
      "range": `M${row}`,
      "majorDimension": "ROWS"
    }
     ,
  })).data;
  console.log(JSON.stringify(response, null, 2));
}
async function listMajors(auth) {
  const sheets = google.sheets({ version: 'v4', auth });
  const res = await sheets.spreadsheets.values.get({
    spreadsheetId: '1ECCS7L4rP0X5s4fY9si0SLv2enoxvlksh1UcGfxsquM',
    range: 'Sheet1!B2:D',
  });
  if (res) {
    const rows = res.data.values;
    if (rows.length) {
      
      // Print columns A and E, which correspond to indices 0 and 4.
      return {
          normal:  rows[0][1],

          fancy:  rows[0][2]
      }
     
    
      
    } else {
      console.log('No data found.');
    }
  }
  return "dff"
}


module.exports = new GoogleSheetApi();
