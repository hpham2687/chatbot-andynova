const { options } = require('../routes/user.route');
const axios = require('../utils/axios');

class ShortenUrl {
    shortenUrl = async (url) => {
        try {
            const config = {
                headers: {
                  'Content-Type': 'application/json',
                  Cookie: '__cfduid=dde96e4c1db70414f099c8e77d7843e981605029222',
                },
              };
              const request = await axios.get(`https://cutt.ly/api/api.php?key=8390bd5aacf835b74787574bf05476d684b51&short=${url}`, config)
            return request.data.url.shortLink;    
        } catch (error) {
            return error.message;
        }
       
    }
}

module.exports = new ShortenUrl();