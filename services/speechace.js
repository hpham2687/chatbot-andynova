const axios = require('../utils/axios');
const request = require('request');
var FormData = require('form-data');

class speechAceApi  {
    checkPronounce = async ({audio, text}) => {


        const url = 'https://ieltsace.com/placement/api/scoring_any_text_speech/';

     
        var data = new FormData();
        data.append('input_data', `{"answer":{"plain_text":"${text}"}}`);
        data.append('user_audio_file', request(audio));
      
        var config = {
          url: 'https://ieltsace.com/placement/api/scoring_any_text_speech/',
          headers: { 
            'Content-Type': 'multipart/form-data', 
            'authorization': 'Token cc9905d0d0f623b68c8883d6c464cbbd7544593e', 
            ...data.getHeaders()
          },
        };
        const response = await axios.post(url, data, config)
           return response.data;    
 

    }

    getToken = async () => {
        const url = 'https://ieltsace.com/s/ielts/anytext/'
        const request = await axios.get(url)
        return request;
    }
}

module.exports = new speechAceApi();
