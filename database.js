const mongoose = require('mongoose');

const URI = process.env.MONGODB_URL;

const db = () => mongoose.connect(URI, {
  useCreateIndex: true,
  useFindAndModify: false,
  useNewUrlParser: true,
  useUnifiedTopology: true,
}, (err) => {
  if (err) {
    console.log(err);
    throw err;
  }
  console.log('connected to mongodb');
});

module.exports = db;
