const userModel = require('../models/users.model');
const writing9Api = require('../services/writing9');
const speechAceApi = require('../services/speechace');
const googleSheetApi = require('../services/googleSheet');
const chatbotConfig = require('../utils/config');

const creditConfig = chatbotConfig.credit;
const responseConfig = chatbotConfig.response;

const userController = {
  register: async (req, res) => {
    try {
      const newUser = new userModel({
        id: req.body.id,
        name: req.body.name,
      });
      console.log('dang ky thang cong');
      return await newUser.save();
    } catch (error) {
      return res.status(500).json({ msg: error.message });
    }
  },
  checkPronunciation: async (req, res) => {
    try {
      const { user } = req;
      const test = 'https://cdn.fbsbx.com/v/t59.3654-21/116819882_727217921444802_5878599994349190764_n.mp4/audioclip-1596120497000-2647.mp4?_nc_cat=109&ccb=2&_nc_sid=7272a8&_nc_ohc=Bms-x_osawwAX8f6Tp1&_nc_ht=cdn.fbsbx.com&oh=f13bea3f707fd549b39d274294013fbf&oe=5FAD96BF&dl=1';
      const data2Send = {
        // text: req.body.custom_fields.new_words,
        text: 'Hello, my name is',
        audio: test,
        // audio: req.body.last_input_text,
      };

      const getScore = await speechAceApi.checkPronounce(data2Send);
      //  res.json(getScore);
      console.log(responseConfig);
      res.json(responseConfig.scoreWord('overall', 'syll', 'phoneme'));
      // tru tien
      user.credit -= creditConfig.creditForPronounce;
      user.pronunciation.numPronunciated++;
      user.save();
      // tru tien
    } catch (error) {
      return res.status(500).json({ msg: error.message });
    }
  },

  checkWriting: async (req, res) => {
    try {
      // console.log(response);
      const { user } = req;
      const data2Send = {
        text: req.body.custom_fields.answer_writing,
        question: req.body.custom_fields.question_writing,
      };
      const urlShortened = await writing9Api.checkWriting(data2Send);

      // tru tien
      user.credit -= creditConfig.creditforCheckWriting;
      user.writing.numChecked++;
      user.save();
      // tru tien

      res.json({ url: urlShortened });
      // res.json({ d: 'dcd' });
    } catch (error) {
      return res.status(500).json({ msg: error.message });
    }
  },
  checkWritingGoogleSheet: async (req, res) => {
    const check = await googleSheetApi.writeResultWriting();
    res.json({ msg: check });
  },
  getNewWord: async (req, res) => {
    const { user } = req;

    // get new word
    const word = await googleSheetApi.getNewWord();
    res.json(word);
    // get new word

    // tru tien
    user.credit -= creditConfig.creditForNewWord;
    user.vocabulary.numLearned++;
    // tru tien
  },
};
module.exports = userController;
