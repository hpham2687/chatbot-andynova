const mongoose = require('mongoose');
const creditConfig = require('../utils/config').credit;

const { Schema } = mongoose;
const userSchema = new Schema({
  id: {
    type: String,
    unique: true,
  }, // String is shorthand for {type: String}
  name: {
    type: String,
    required: [true, 'Why no name?'],
  },
  credit: {
    type: Number,
    default: creditConfig.defaultCreditWhenRegister,
  },
  dateCreated: { type: Date, default: Date.now },
  writing: {
    numChecked: {
      type: Number,
      default: 0,
    },
  },
  vocabulary: {
    numLearned: {
      type: Number,
      default: 0,
    },
    reminder: [
      {
        word: {
          type: String,
          default: 'Hello world',
        },
        timeToRemind: { type: Date, default: Date.now },
      },
    ],
  },
  pronunciation: {
    numPronunciated: {
      type: Number,
      default: 0,
    },
  },
});

const UserModel = mongoose.model('users', userSchema);

module.exports = UserModel;
