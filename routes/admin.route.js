const express = require('express');

const router = express.Router();

router.get('/database/reset', (req, res) => {
  res.send('Birds home page');
});

module.exports = router;
