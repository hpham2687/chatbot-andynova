const express = require('express');

const router = express.Router();
const userController = require('../controllers/user.controller');

const creditMiddleware = require('../middlewares/credit.middleware');
const authMiddleware = require('../middlewares/auth.middleware');

router.post('/register', userController.register);
router.post('/checkPronunciation', authMiddleware, creditMiddleware, userController.checkPronunciation);
router.get('/getNewWord', userController.getNewWord);

router.post('/checkWriting', authMiddleware, creditMiddleware, userController.checkWriting);

router.get('/checkWritingGoogleSheet', userController.checkWritingGoogleSheet);

module.exports = router;
