module.exports = {
  credit: {
    defaultCreditWhenRegister: 10,
    creditForPronounce: 1,
    creditForNewWord: 1,

    creditforCheckWriting: 2,
  },
  response: {
    creditExhaust: {
      version: 'v2',
      content: {
        messages: [
          {
            type: 'text',
            text: 'Hic, bạn sử dụng hết credit mất rồi. Vui lòng like share để tích credit nha! Nạp credit để dùng tiếp nha',
          },
          {
            type: 'text',
            text: 'Hoặc bạn có code? Bạn có thể nhập code để cộng điểm!',
            buttons: [
              {
                type: 'flow',
                caption: 'OK go',
                target: 'content20200420080805_568909',
              },
              {
                type: 'flow',
                caption: 'Không có',
                target: 'content20200420161222_588999',
              },
            ],
          },
        ],
        actions: [],
        quick_replies: [],

      },
    },
    scoreWord: (overall_score, syll, phoneme) => ({
      version: 'v2',
      content: {
        messages: [
          {
            type: 'text',
            text: `Overall Score: ${overall_score} \nGia Cát Dự Điểm IELTS: '.$ielts_score.'\nPTE Score'.$pte_score.'`,
          },
          {
            type: 'text',
            text: `Syllables: \n ${syll}`,
          },
          {
            type: 'text',
            text: `Phonemes: \n ${phoneme}`,
          },
          {
            type: 'video',
            url: 'https://manybot-files.s3.eu-central-1.amazonaws.com/fb1704493459774994/ca/2020/04/20/b48a9eb7f11f3fad4900e10f0ac512a1/video-1587390741.mp4',
            buttons: [],
          },
          {
            type: 'text',
            text: 'Chúc mừng bạn đã là Cao thủ Phát âm với câu này! Các bạn hãy thử các mẫu câu dài và khó hơn nha',
          },
          {
            type: 'text',
            text: 'bạn có muốn chơi tiếp không?',
            buttons: [
              {
                type: 'flow',
                caption: 'Có',
                target: 'content20200420063316_320698',
              },
              {
                type: 'flow',
                caption: 'Không',
                target: 'content20200420161222_588999',
              },
            ],
          },

        ],
      },
    }),

  },
};
