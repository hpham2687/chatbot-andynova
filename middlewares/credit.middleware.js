const chatbotConfig = require('../utils/config');

const responseConfig = chatbotConfig.response;
const creditMiddleware = async (req, res, next) => {
  try {
    const { credit } = req.user;
    console.log(credit);
    if (credit === 0) {
      res.json(responseConfig.creditExhaust);
    } else {
      next();
    }
  } catch (error) {
    return res.status(500).json({ msg: error.message });
  }
};

module.exports = creditMiddleware;
