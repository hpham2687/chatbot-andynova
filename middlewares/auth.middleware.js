const userModel = require('../models/users.model');
const userController = require('../controllers/user.controller');

const authMiddleware = async (req, res, next) => {
  try {
    const { id } = req.body;

    let user = await userModel.findOne({ id });

    if (user) {
      console.log('ton tai, next');

      req.justRegister = false;
    } else {
      console.log('ko ton tai. vao dang ky');
      req.justRegister = true;
      user = await userController.register(req, res);
    }
    req.user = user;

    next();
  } catch (error) {
    return res.status(500).json({ msg: error.message });
  }
};

module.exports = authMiddleware;
