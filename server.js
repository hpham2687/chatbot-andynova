require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const userRouter = require('./routes/user.route');

const db = require('./database');
const UserModel = require('./models/users.model');

const app = express();

const jsonParser = bodyParser.json();

const port = process.env.PORT || 3000;

db();
app.use(jsonParser);
app.use('/user', userRouter);

app.get('/', (req, res) => {
  const newUser = new UserModel({
    name: 'hihi',
    id: 'hihi',
  });

  newUser.save();

  res.send('Hello World!');
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
